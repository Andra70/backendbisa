<?php

namespace App\Models;

use App\Models\Question;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

// class Quiz extends Model
// {
//     use HasFactory;

//     public function questions()
//     {
//         return $this->hasMany(Question::class);
//     }

//     protected $fillable = [
//         'name', 
//         'status',
//         'rating',
//     ];

//     protected $casts = [
//         'rating' => 'integer',
//     ];

//     public function questions()
//     {
//         return $this->hasMany(Question::class);
//     }
// //     public function users()
// // {
// //     return $this->belongsToMany(User::class);
// // }

// }

class Quiz extends Model
{
    use HasFactory;

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    protected $fillable = [
        'name', 
        'status',
        'difficulty',
    ];

    // protected $casts = [
    //     'rating' => 'integer',
    // ];

//     public function questions()
//     {
//         return $this->hasMany(Question::class);
//     }
// //     public function users()
// // {
// //     return $this->belongsToMany(User::class);
// // }

}
