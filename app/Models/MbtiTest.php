<?php

namespace App\Models;

use App\Models\Quiz;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MbtiTest extends Model
{
    use HasFactory;
    

    protected $fillable = [
        'soal',
        'answers',
        'mbti_type',
        'score',
    ];

    protected $table = 'mbti_tests';

    protected $casts = [
        'score' => 'integer',
    ];

    public function test()
    {
        return $this->belongsTo(Quiz::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
