<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        // 'quiz_id',
        'text',
        'option_a',
        'option_b',
        'correct_answer',
        // 'quiz_id'
    ];

    // protected $primaryKey = 'question_id';

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}
