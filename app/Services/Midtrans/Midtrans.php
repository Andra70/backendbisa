<?php
 
namespace App\Services\Midtrans;
 
use Midtrans\Config;
 
class Midtrans {
    protected $serverKey;
    protected $isProduction;
    protected $isSanitized;
    protected $is3ds;
 
    public function __construct()
    {
        $this->serverKey = config('app.server_key');
        $this->isProduction = config('app.is_production');
        $this->isSanitized = config('app.is_sanitized');
        $this->is3ds = config('app.is_3ds');
 
        $this->_configureMidtrans();
    }
 
    public function _configureMidtrans()
    {
        Config::$serverKey = $this->serverKey;
        Config::$isProduction = $this->isProduction;
        Config::$isSanitized = $this->isSanitized;
        Config::$is3ds = $this->is3ds;
    }
}