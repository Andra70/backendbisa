<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\JobCategory;
use App\Models\Test;
use App\Models\Mahasiswa;
use App\Models\Quiz;

class DashboardController extends Controller
{
    public function index()
    {
        $profiles = Profile::all();
        $jobCategories = JobCategory::all();
        $quizzes = Quiz::all();
        $mahasiswas = Mahasiswa::all();


        return view('index', compact('profiles', 'jobCategories', 'quizzes'));
    }
    
}
