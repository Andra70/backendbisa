<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Midtrans\CallbackService;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_name' => 'required',
            // 'user_id'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => true,
                'message' => $validator->errors(),
                'data' => null
            ]);
        }

        $subscription = $request->subscription_id;
        $subs = Subscription::where('id', $subscription)->first();

        $order = Order::create([
            'subscription_code' => 'BA-' . substr(str_shuffle(md5(time())), 0, 10),
            'order_name' => $request->order_name,
            // 'user_id'=>Auth::user()->id,
            'subscription_id' => $subscription,
            'total' => $subs->price,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Order berhasil dibuat',
            'data' => $order,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function paymentStore(Request $request,$id)
    {
        $order = Order::find($id);

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('app.server_key');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id' => $order->id.uniqid(),
                'gross_amount' => $order->total,
            ),
            'customer_details' => array(
                'first_name' => $order->order_name,
                // 'last_name' => 'pratama',
                // 'email' => 'budi.pra@example.com',
                // 'phone' => '08111222333',
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);
        $redirect_url = 'https://app.sandbox.midtrans.com/snap/v2/vtweb/' . $snapToken;

        return response()->json([
            'success'   => True,
            'message'    => 'successfully created payment',
            'data' => [
                'token' => $snapToken,
                'redirect_url' => $redirect_url,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    public function callback(string $id)
    {
        $order = Order::find($id);
        if (!$order) {
            // Pesanan tidak ditemukan
            return response()->json([
                'success' => false,
                'message' => 'Order tidak ditemukkan'
            ], 404);
        }

        if ($order->status === 'unpaid') {
            // Ubah status pesanan menjadi "paid"
            $order->status = 'paid';
            $order->save();
            // Melalkukan tindakan setelah pembayaran
            return response()->json([
                'success' => true,
                'message' => 'Payment berhasil diubah ke paid'
            ], 200);
        } else {
            // Pesanan telah dibayar sebelumnya
            return response()->json([
                'success' => false,
                'message' => 'Order belum dibayar'
            ], 400);
        }
    }

    public function receive()
    {
        
        $callback = new CallbackService;
 
        if ($callback->isSignatureKeyVerified()) {
            $notification = $callback->getNotification();
            $order = $callback->getOrder();
            if ($callback->isSuccess()) {

        Order::where('id', $order->id)->update([
            'status' => 'paid',
        ]);
            }
 
            if ($callback->isExpire()) {
                Order::where('id', $order->id)->update([
                    'status' => 'expire',
                ]);
            }
 
            if ($callback->isCancelled()) {
                Order::where('id', $order->id)->update([
                    'status' => 'cancel',
                ]);
            }
 
            return response()
                ->json([
                    'success' => true,
                    'message' => 'Notifikasi berhasil diproses',
                ]);
        } else {
            return response()
                ->json([
                    'error' => true,
                    'message' => 'Signature key tidak terverifikasi',
                ], 403);
        }
    }

    public function finish(){
        return redirect()->route('afterorder');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
