<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Resources\CommentResource;
use App\Http\Resources\PostDetailResource;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
           'post_id' => 'required|exists:posts,id', 
           'comments_content' => 'required',    
        ]);

        $request = $request->merge(['user_id' => auth()->user()->id]);
        $comment = Comment::create($request->all());
        // return new PostDetailResource($post->loadMissing('writer:id,username'));

        return new CommentResource($comment->loadMissing(['comentator:id,username']), 200); 
    }

    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return response()->json(['message' => 'Comment deleted successfully'], 200);
        return new CommentResource($comment->loadMissing(['comentator:id,username'],200));
    }
}