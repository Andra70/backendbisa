<?php

namespace App\Http\Controllers;

use App\Models\Story;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $stories = Story::all();
        return response()->json(['message' => 'Success', 'data' => $stories], 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust validation rules
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust validation rules
            'images' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Adjust validation rules
            'description' => 'required',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = now()->timestamp . '.' . $image->getClientOriginalExtension();
            $path = public_path('storage/images/');
            $image->move($path, $imageName);
            $imagePath = 'storage/images/' . $imageName;
        }
        $story = Story::create([
            'name' => $request->name,
            'logo' => $imageName,
            'image' => $imageName,
            'images' => $imageName,
            'description' => $request->description,
        ]);

        return response()->json(['message' => 'Story created successfully', 'data' => $story], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Story $story)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Story $story, $id)
    {
        $story = Story::findOrFail($id);

        // Delete the associated image from storage
        Storage::disk('public')->delete($story->image);

        // Delete the story record from the database
        $story->delete();

        return response()->json(['message' => 'Story deleted successfully'], 200);
    }
}
