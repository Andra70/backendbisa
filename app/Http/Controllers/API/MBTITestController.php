<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MBTITestController extends Controller
{
    private function calculateScore($answers)
    {
        $score = 0;

        // Contoh penghitungan skor berdasarkan jawaban
        foreach (str_split($answers) as $answer) {
            switch ($answer) {
                case 'E':
                    $score += 1;
                    break;
                case 'I':
                    $score -= 1;
                    break;
                case 'N':
                    $score += 1;
                    break;
                case 'S':
                    $score -= 1;
                    break;
                case 'T':
                    $score += 1;
                    break;
                case 'F':
                    $score -= 1;
                    break;
                case 'J':
                    $score += 1;
                    break;
                case 'P':
                    $score -= 1;
                    break;
                // Tambahkan case untuk jawaban lain sesuai kebutuhan
            }
        }

        return $score;
    }

    private function calculateMBTIType($score)
    {
        if ($score > 0) {
            return 'E';
        } else {
            return 'I';
        } if ($score > 0) {
            return 'N';
        } else {
            return 'S';
        } if ($score > 0) {
            return 'T';
        } else {
            return 'F';
        } if ($score > 0) {
            return 'J';
        } else {
            return 'P';
        }
        // Tambahkan aturan berdasarkan skor lainnya sesuai kebutuhan
    }
    
    public function create()
    {
        return view('mbti_test.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'soal' => 'required|string',
            'answers' => 'required|string',
            'answer' => 'required|string',
            'mbti_type' => 'required|string',
            'score' => 'required|integer',
        ]);

        $score = $this->calculateScore($request->input('answers'));
        $mbtiType = $this->calculateMBTIType($score);


        // $test = new MBTITest();
        // $test->soal = $request->input('soal');
        // $test->answers = $request->input('answers');
        // $test->answers = $request->input('answer');
        // $test->score = $score;
        // $test->mbti_type = $mbtiType;
        // $test->save();

        $test = MBTITest::create([
            'soal' => $soal,
            'answers' => $aswers,
            'answer' => $answer,
            'mbti_type' => $mbtiType,
            'score' => $score,
        ]);

        return redirect()->route('mbti_test.show', $test->id);
    }
}
