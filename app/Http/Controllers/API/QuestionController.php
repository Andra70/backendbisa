<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Quiz;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        return response()->json([
            'success' => true,
            'message' => 'Questions retrieved successfully',
            'data' => $questions
        ]);
    }

    public function store(Request $request)
    {
        // Validation logic here
        $request->validate([
            'quiz_id' => 'required',
            'text' => 'required|string',
            'option_a' => 'required|string',
            'option_b' => 'required|string',
            // 'correct_answer' => 'required|string',

        ]);

        $question = new Question();
        $question->quiz_id = $request->input('quiz_id');
        $question->text = $request->input('text');
        $question->option_a = $request->input('option_a');
        $question->option_b = $request->input('option_b');
        // $question->correct_answer = $request->input('correct_answer');

        $question->save();

        return response()->json([
            'success' => true,
            'message' => 'Question created successfully',
            'data' => $question
        ]);
    }

    public function show($id)
    {
        $question = Question::find($id);

        if (!$question) {
            return response()->json([
                'success' => false,
                'message' => 'Question not found',
                'data' => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Question retrieved successfully',
            'data' => $question
        ]);
    }

    public function update(Request $request, $id)
    {
        // Validation logic here
        $request->validate([
            'text' => 'required|string',
            'option_a' => 'required|string',
            'option_b' => 'required|string',
            // 'option_c' => 'required|string',
            // 'option_d' => 'required|string',
            // 'correct_answer' => 'required|string',

        ]);

        $question = Question::find($id);

        if (!$question) {
            return response()->json([
                'success' => false,
                'message' => 'Question not found',
                'data' => null
            ], 404);
        }

        $question->text = $request->input('text');
        $question->option_a = $request->input('option_a');
        $question->option_b = $request->input('option_b');
        // $question->option_c = $request->input('option_c');
        // $question->option_d = $request->input('option_d');
        // $question->correct_answer = $request->input('correct_answer');

        $question->save();

        return response()->json([
            'success' => true,
            'message' => 'Question updated successfully',
            'data' => $question
        ]);
    }

    public function destroy($id)
    {
        $question = Question::find($id);

        if (!$question) {
            return response()->json([
                'success' => false,
                'message' => 'Question not found',
                'data' => null
            ], 404);
        }

        $question->delete();

        return response()->json([
            'success' => true,
            'message' => 'Question deleted successfully',
            'data' => $question
        ]);
    }

}
