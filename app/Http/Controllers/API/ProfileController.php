<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $profiles = Profile::all();

        return response()->json([
            'success' => true,
            'message' => 'Profiles retrieved successfully',
            'data' => $profiles
        ]);
    }

    //make me new function form
    public function create()
    {
        return view('dashboard.profile.create');
    }

    public function showProfilesPage()
    {
        $profiles = Profile::all();
        return view('index')->with('profiles', $profiles);
    }

    public function store(Request $request)
    {
        // Validasi input
        $request->validate([
            'full_name' => 'required|string',
            'school_name' => 'required|string',
            'class' => 'required|string',
            'address' => 'required|string',
            'phone_number' => 'required|string',
            'email' => 'required|string|email',
            'about_me' => 'nullable|string',
        ]);

        $profile = new Profile();
        $profile->full_name = $request->input('full_name');
        $profile->school_name = $request->input('school_name');
        $profile->class = $request->input('class');
        $profile->address = $request->input('address');
        $profile->phone_number = $request->input('phone_number');
        $profile->email = $request->input('email');
        $profile->about_me = $request->input('about_me');
        $profile->save();

        if ($request->wantsJson()) {
            // API scenario
            return response()->json([
                'success' => true,
                'message' => 'Profile created successfully',
                'data' => $profile
            ]);
        } else {
            // Web view scenario
            return redirect()->route('profiles.create')->with('success', 'Profile created successfully.');
        }
    }

    public function show($id)
    {
        $profile = Profile::find($id);

        if (!$profile) {
            return response()->json([
                'success' => false,
                'message' => 'Profile not found',
                'data' => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Profile retrieved successfully',
            'data' => $profile
        ]);
    }

    public function update(Request $request, $id)
    {
        // Validasi input
        $request->validate([
            'full_name' => 'required|string',
            'school_name' => 'required|string',
            'class' => 'required|string',
            'address' => 'required|string',
            'phone_number' => 'required|string',
            'email' => 'required|string|email',
            'about_me' => 'nullable|string',
        ]);

        $profile = Profile::find($id);

        if (!$profile) {
            return response()->json([
                'success' => false,
                'message' => 'Profile not found',
                'data' => null
            ], 404);
        }

        $profile->full_name = $request->input('full_name');
        $profile->school_name = $request->input('school_name');
        $profile->class = $request->input('class');
        $profile->address = $request->input('address');
        $profile->phone_number = $request->input('phone_number');
        $profile->email = $request->input('email');
        $profile->about_me = $request->input('about_me');
        $profile->save();

        return response()->json([
            'success' => true,
            'message' => 'Profile updated successfully',
            'data' => $profile
        ]);
    }

    public function edit(Profile $profiles)
    {
        return view('dashboard.profile.update')->with('profile',$profiles);
    }

    public function destroy($id)
    {
        $profile = Profile::find($id);

        if (!$profile) {
            return response()->json([
                'success' => false,
                'message' => 'Profile not found',
                'data' => null
            ], 404);
        }

        $profile->delete();

        return response()->json([
            'success' => true,
            'message' => 'Profile deleted successfully'
        ]);
    }
}
