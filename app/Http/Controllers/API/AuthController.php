<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone_number' => 'required|string|unique:users', // Add phone number validation here
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        // $input['user_uuid'] = (string) Str::uuid();
        $user = User::create($input);

        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        // $success['name'] = $user->name;
        $success['email'] = $user->email;
        $success['username'] = $user->username;
        $success['phone_number'] = $user->phone_number; 

        return response()->json([
            'success' => true,
            'message' => 'User registered successfully',
            'data' => $success
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $user = User::where('username', $request->username)->first();

        if ($user && Auth::attempt(['username' => $request->username, 'password' => $request->password])) {

            $success['token'] = $user->createToken('auth_token')->plainTextToken;
            $success['username'] = $user->username;
            $success['email'] = $user->email;
            $success['phone_number'] = $user->phone_number;
            // $success['username'] = $user->username;

            return response()->json([
                'success' => true,
                'message' => 'User logged in successfully',
                'data' => $success
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Invalid email or password',
                'data' => null
            ], 401);
        }
    }


    public function logout(Request $request)
    {
        $token = $request->user()->currentAccessToken();
        if ($token) {
            $token->delete();
            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Failed to logout',
                'data' => null
            ], 500);
        }
    }


    public function protectedResource(Request $request)
    {
        // Check if the user is authenticated
        if (auth()->user()) {
            // User is authenticated, return the protected data
            return response()->json([
                'success' => true,
                'message' => 'Access to protected resource granted',
                'data' => [
                    // Your protected data here
                ]
            ]);
        } else {
            // User is not authenticated, return an unauthorized response
            return response()->json([
                'success' => false,
                'message' => 'Unauthorized',
                'data' => null
            ], 401);
        }
    }
}
