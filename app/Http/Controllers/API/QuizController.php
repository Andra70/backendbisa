<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quiz;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    public function index()
    {
        $quizzes = Quiz::all();

        return response()->json([
            'success' => true,
            'message' => 'Quizs retrieved successfully',
            'data' => $quizzes
        ]);

        // $quizzes = Quiz::with('questions')->get();

        // $formattedQuizzes = $quizzes->map(function ($quiz) {
        //     $formattedQuiz = $quiz->toArray();
        //     $formattedQuiz['id_question:' . $quiz->id] = $formattedQuiz['questions'];
        //     unset($formattedQuiz['questions']);
        //     return $formattedQuiz;
        // });

        // return response()->json([
        //     'success' => true,
        //     'message' => 'Quizzes retrieved successfully',
        //     'data' => $formattedQuizzes
        // ]);
    }

    // public function showWithQuestions($id)
    // {
    //     $quiz = Quiz::with('questions')->find($id);

    //     if (!$quiz) {
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Quiz not found',
    //             'data' => null
    //         ], 404);
    //     }

    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Quiz with questions retrieved successfully',
    //         'data' => $quiz
    //     ]);
    // }

    //make me function showQuiz
    public function showQuizPage()
    {
        $quizzes = Quiz::all();
        return view('index')->with('Quizs', $quizzes);
    }

    public function store(Request $request)
    {
        // Validasi input
        $request->validate([
            'name' => 'required|string',
            'status' => 'required|string',
            'difficulty' => 'required|string',
        ]);

        $quiz = new Quiz();
        $quiz->name = $request->input('name');
        $quiz->status = $request->input('status');
        $quiz->difficulty = $request->input('difficulty');
        $quiz->save();

        return response()->json([
            'success' => true,
            'message' => 'Quiz created successfully',
            'data' => $quiz
        ]);
    }

    public function show($id)
    {
        $quiz = Quiz::find($id);

        if (!$quiz) {
            return response()->json([
                'success' => false,
                'message' => 'Quiz not found',
                'data' => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Quiz retrieved successfully',
            'data' => $quiz
        ]);
    }

    public function update(Request $request, $id)
    {
        // Validasi input
        $request->validate([
            'name' => 'required|string',
            'status' => 'required|string',
            'difficulty' => 'required|string',
        ]);

        $quiz = Quiz::find($id);

        if (!$quiz) {
            return response()->json([
                'success' => false,
                'message' => 'Quiz not found',
                'data' => null
            ], 404);
        }

        $quiz->name = $request->input('name');
        $quiz->status = $request->input('status');
        $quiz->difficulty = $request->input('difficulty');
        $quiz->save();

        return response()->json([
            'success' => true,
            'message' => 'Quiz updated successfully',
            'data' => $quiz
        ]);
    }

    public function destroy($id)
    {
        $quiz = Quiz::find($id);

        if (!$quiz) {
            return response()->json([
                'success' => false,
                'message' => 'Quiz not found',
                'data' => null
            ], 404);
        }

        $quiz->delete();

        return response()->json([
            'success' => true,
            'message' => 'Quiz deleted successfully'
        ]);
    }

//     public function startQuiz(Request $request, Quiz $quiz)
//     {
//         $user = Auth::user();
//         $user->quizzes()->syncWithoutDetaching([$quiz->id]);

//         return response()->json([
//             'success' => true,
//             'message' => 'Quiz started',
//         ]);
//     }

//     public function userQuizzes(Request $request)
//     {
//         $user = Auth::user();
//         $quizzes = $user->quizzes;

//         return response()->json([
//             'success' => true,
//             'message' => 'User quizzes retrieved successfully',
//             'data' => $quizzes,
//         ]);
//     }

//     protected function jsonResponse($success, $message, $data = null, $statusCode = 200)
// {
//     return response()->json([
//         'success' => $success,
//         'message' => $message,
//         'data' => $data,
//     ], $statusCode);
// }
    
}
