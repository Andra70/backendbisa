<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Answer;

class AnswerController extends Controller
{
    public function store(Request $request)
    {
        // Validate the incoming data
        $request->validate([
            'question_id' => 'required',
            'answer' => 'required|string',
            // 'mbti_type' => 'required'
        ]);

        // Create a new Answer instance and save it to the database
        $answer = new Answer();
        $answer->question_id = $request->input('question_id');
        $answer->answer = $request->input('answer');
        // $answer->mbti_type = $request->input('mbti_type');

        $answer->save();

        return response()->json([
            'success' => true,
            'message' => 'Answer submitted successfully',
            'data' => $answer
        ]);
    }
}
