<?php

namespace App\Http\Controllers\API_ARTIKEL;

use App\Models\Post;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\PostDetailResource;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();   
        // return response()->json(['data' => $posts]);
        return PostDetailResource::collection($posts->loadMissing('writer:id,username','comments:id,post_id,user_id,comments_content'));
    }
    public function show($id)
    {
        $posts = Post::with('writer:id,username')->findOrFail($id);
        return new PostDetailResource($posts->loadMissing('writer:id,username', 'comments:id,post_id,user_id,comments_content'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required||max:255',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = now()->timestamp . '.' . $image->extension();
            $path = public_path('storage/images/');
            $image->move($path, $imageName);
            $image = 'images/' . $imageName;
        }
        $posts = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $image,
            'author' => Auth::user()->id,
        ]);
        return new PostDetailResource($posts->loadMissing('writer:id,username'), 201);

    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required||max:255',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = now()->timestamp . '.' . $image->getClientOriginalExtension();
            $path = public_path('storage/images/');
            $image->move($path, $imageName);
            $imagePath = 'storage/images/' . $imageName;
        }

        $post = Post::findOrFail($id);
        $post->update([
            'title' => $request->title,
            'content' => $request->content,
            'image' => $imageName,
        ]
    );
        return new PostDetailResource($post->loadMissing('writer:id,username'), 201);
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return response()->json(['message' => 'Post deleted'], 200);    
    }
}
 