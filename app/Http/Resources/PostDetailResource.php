<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => asset('storage/images/'.$this->image),
            'content' => $this->content,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'author' => $this->author,
            'writer' => $this->whenLoaded('writer'),
            'comments' => CommentResource::collection($this->whenLoaded('comments',
             $this->comments->loadMissing('comentator:id,username'))),
        ];
    }
}
