<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

// CRUD
use App\Http\Controllers\StoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\QuizController;
use App\Http\Controllers\API\AnswerController;



// ARTIKEL
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\QuestionController;
use App\Http\Controllers\API\MahasiswaController;
use App\Http\Controllers\API\JobCategoryController;
use App\Http\Controllers\API_ARTIKEL\PostController;



//CRUD
Route::get('mahasiswa',[MahasiswaController::class, 'index']);
Route::post('mahasiswa/store',[MahasiswaController::class, 'store']);
Route::get('mahasiswa/show/{id}',[MahasiswaController::class, 'show']);
Route::post('mahasiswa/update/{id}',[MahasiswaController::class, 'update']);
Route::get('mahasiswa/destroy/{id}',[MahasiswaController::class, 'destroy']);

// Authentication
Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->post('/logout', [AuthController::class, 'logout']);
Route::middleware('auth:sanctum')->get('protected-resource', [AuthController::class, 'protectedResource']);

// Job Category
Route::get('job-categories', [JobCategoryController::class, 'index']);
Route::post('job-categories/store', [JobCategoryController::class, 'store']);
Route::get('job-categories/show/{id}', [JobCategoryController::class, 'show']);
Route::post('job-categories/update/{id}', [JobCategoryController::class, 'update']);
Route::delete('job-categories/destroy/{id}', [JobCategoryController::class, 'destroy']);

// Profile Page
Route::get('profiles', [ProfileController::class, 'index']);
Route::post('profiles/create', [ProfileController::class, 'store']);
Route::get('profiles/show/{id}', [ProfileController::class, 'show']);
Route::post('profiles/update/{id}', [ProfileController::class, 'update']);
Route::delete('profiles/destroy/{id}', [ProfileController::class, 'destroy']);

// quiz
Route::get('quizzes', [QuizController::class, 'index']);
Route::post('quizzes/create', [QuizController::class, 'store']);
Route::get('quizzes/show/{id}', [QuizController::class, 'show']);
Route::put('quizzes/update/{id}', [QuizController::class, 'update']);
Route::delete('quizzes/destroy/{id}', [QuizController::class, 'destroy']);

Route::post('quizzes/{quiz}/start', [QuizController::class, 'startQuiz']);
Route::get('user/quizzes', [QuizController::class, 'userQuizzes']);

//questions
Route::get('questions', [QuestionController::class, 'index']);
Route::post('questions/create', [QuestionController::class, 'store']);
Route::get('questions/show/{id}', [QuestionController::class, 'show']);
Route::put('questions/update/{id}', [QuestionController::class, 'update']);
Route::delete('questions/destroy/{id}', [QuestionController::class, 'destroy']);

Route::get('/quizzes/{id}/with-questions', [QuizController::class, 'showWithQuestions']);

Route::resource('quizzes', QuizController::class);
Route::resource('quizzes.questions', QuestionController::class);

Route::post('answers/create', [AnswerController::class, 'store']);

// Route::group(['middleware' => ['auth:sanctum']], function () {
//     Route::get('user/quizzes', [QuizController::class, 'userQuizzes']);
//     // Add more protected routes here
// });

// Route::prefix('quizzes')->group(function () {
//     Route::get('/', [QuizController::class, 'index']);
//     Route::post('/', [QuizController::class, 'store']);
//     Route::get('/{quiz}', [QuizController::class, 'show']);
//     Route::put('/{quiz}', [QuizController::class, 'update']);
//     Route::delete('/{quiz}', [QuizController::class, 'destroy']);
//     Route::post('/{quiz}/start', [QuizController::class, 'startQuiz']);
// });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// ARTIKEL 
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/posts', [PostController::class, 'index']);
    Route::get('/posts/{id}', [PostController::class, 'show']);
    Route::post('/posts', [PostController::class, 'store']);
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::post('/posts/{id}', [PostController::class, 'update']);
    Route::delete('/posts/{id}', [PostController::class, 'destroy']);
    Route::post('/comment', [CommentController::class, 'store']);
    Route::delete('/comment/{id}', [CommentController::class, 'destroy']);

});

// STORY
Route::get('/story', [StoryController::class, 'index']);
Route::post('/story/create', [StoryController::class, 'store']);
Route::delete('/story/{id}', [StoryController::class, 'destroy']);

// PAYMENT
Route::get('/', [AppController::class, 'index']);
Route::post('/order', [OrderController::class, 'create']);
Route::get('/payment/{id}', [OrderController::class, 'paymentStore']);
Route::post('/callback/{id}', [OrderController::class, 'callback']);
Route::post('/payments/midtrans-notification', [OrderController::class, 'receive']);