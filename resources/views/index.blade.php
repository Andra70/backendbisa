@extends('dashboard.layouts.main')

@section('content')
<div class="container-fluid" id="container-wrapper">
    <!-- Dashboard Header -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </div>

    <!-- Profiles Table -->
    <h2>Profiles</h2>
    <a href="{{ route('profiles.create') }}" class="btn btn-primary">Add New Profile</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Full Name</th>
                <th>School Name</th>
                <th>Class</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Email Address</th>
                <th>About Me</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($profiles as $profile)
            <tr>
                <td>{{ $profile->id }}</td>
                <td>{{ $profile->full_name }}</td>
                <td>{{ $profile->school_name }}</td>
                <td>{{ $profile->class }}</td>
                <td>{{ $profile->address }}</td>
                <td>{{ $profile->phone_number }}</td>
                <td>{{ $profile->email }}</td>
                <td>{{ $profile->about_me }}</td>
                <td>
                    <a href="{{ route('profiles.show', ['id' => $profile->id]) }}" class="btn btn-info">View</a>
                    <a href="{{ route('profiles.edit', ['id' => $profile->id]) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('profiles.destroy', ['id' => $profile->id]) }}" method="POST" class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this profile?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


    <!-- Job Categories Table -->
    <table class="table">
        <h2>Job Categories</h2>
        <!-- Add New Job Category Button -->
        <a href="{{ route('job_categories.create') }}" class="btn btn-primary">Add New Job Category</a>
        <!-- Table Header -->
        <thead>
            <tr>
                <th>ID</th>
                <th>Job Category</th>
                <th>Action</th>
            </tr>
        </thead>
        <!-- Table Body -->
        <tbody>
            @foreach ($jobCategories as $jobCategory)
            <tr>
                <td>{{ $jobCategory->id }}</td>
                <td>{{ $jobCategory->name }}</td>
                <td>
                    <a href="{{ route('job_categories.show', ['id' => $jobCategory->id]) }}" class="btn btn-info">View</a>
                    <!-- Edit Job Category Link -->
                    <a href="{{ route('job_categories.edit', ['job_category' => $jobCategory->id]) }}" class="btn btn-primary">Edit</a>
                    <!-- Delete Job Category Form -->
                    <form action="{{ route('job_categories.destroy', ['job_category' => $jobCategory->id]) }}" method="POST" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this job category?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <!-- quizzes Table -->
    <table class="table">
        <h2>quizzes</h2>
        <!-- Add New quiz Button -->
        <a href="{{ route('quizzes.store') }}" class="btn btn-primary">Add New quiz</a>
        <!-- Table Header -->
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Status</th>
                <th>Rating</th>
                <th>Action</th>
            </tr>
        </thead>
        <!-- Table Body -->
        <tbody>
            @foreach ($quizzes as $quiz)
            <tr>
                <td>{{ $quiz->id }}</td>
                <td>{{ $quiz->name }}</td>
                <td>{{ $quiz->status }}</td>
                <td>{{ $quiz->rating }}</td>
                <td>
                    <!-- Edit quiz Link -->
                    <a href="{{ route('quizzes.update', ['quiz' => $quiz->id]) }}" class="btn btn-primary">Edit quiz</a>
                    <!-- Delete quiz Form -->
                    <form action="{{ route('quizzes.destroy', ['quiz' => $quiz->id]) }}" method="POST" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this quiz?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


    <!-- Modal Logout and other scripts -->
</div>
@endsection