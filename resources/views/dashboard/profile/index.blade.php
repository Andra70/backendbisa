@extends('dashboard.layouts.main')

@section('content')

<h2>Profiles</h2>
    <a href="{{ route('profiles.store') }}" class="btn btn-primary">Add New Profile</a>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Full Name</th>
                <th>School Name</th>
                <th>Class</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Email Address</th>
                <th>About Me</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($profiles as $profile)
            <tr>
                <td>{{ $profile->id }}</td>
                <td>{{ $profile->full_name }}</td>
                <td>{{ $profile->school_name }}</td>
                <td>{{ $profile->class }}</td>
                <td>{{ $profile->address }}</td>
                <td>{{ $profile->phone_number }}</td>
                <td>{{ $profile->email }}</td>
                <td>{{ $profile->about_me }}</td>
                <td>
                    <a href="{{ route('profiles.show', ['id' => $profile->id]) }}" class="btn btn-info">View</a>
                    <a href="{{ route('profiles.edit', ['id' => $profile->id]) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('profiles.destroy', ['id' => $profile->id]) }}" method="POST" class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this profile?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>