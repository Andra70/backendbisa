@extends('dashboard.layouts.main')

@section('content')

<form action="{{ route('profiles.store') }}" method="POST">
    @csrf
    <!-- Form fields for profile properties -->
    <div class="form-group">
        <label for="full_name">Full Name</label>
        <input type="text" class="form-control" id="full_name" name="full_name" required>
    </div>
    <!-- Add other input fields here for other profile properties -->
    <div class="form-group">
        <label for="school_name">School Name</label>
        <input type="text" class="form-control" id="school_name" name="school_name" required>
    </div>
    <div class="form-group">
        <label for="class">Class</label>
        <input type="text" class="form-control" id="class" name="class" required>
    </div>
    <div class="form-group">
        <label for="address">Address</label>
        <input type="text" class="form-control" id="address" name="address" required>
    </div>
    <div class="form-group">
        <label for="phone_number">Phone Number</label>
        <input type="text" class="form-control" id="phone_number" name="phone_number" required>
    </div>
    <div class="form-group">
        <label for="email">Email Address</label>
        <input type="text" class="form-control" id="email" name="email" required>
    </div>
    <div class="form-group">
        <label for="about_me">About Me</label>
        <input type="text" class="form-control" id="about_me" name="about_me" required>
    </div>
    <!-- Submit button -->
    <button type="submit" class="btn btn-primary">Create Profile</button>
</form>
@endsection



