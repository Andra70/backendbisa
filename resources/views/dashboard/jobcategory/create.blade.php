@extends('dashboard.layouts.main')

@section('content')

<form action="{{ route('job_categories.store') }}" method="POST">
    @csrf
    <!-- Form fields for profile properties -->
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required>
    </div>
    <!-- Submit button -->
    <button type="submit" class="btn btn-primary">Create Category</button>
</form>
@endsection



