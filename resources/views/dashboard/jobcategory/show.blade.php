@extends('dashboard.layouts.main')

@section('content')

<h5 class="card-title">ID: {{ $jobCategory->id }}</h5>
<h5 class="card-title">Name: {{ $jobCategory->name }}</h5>
<a href="{{ route('job_categories.index') }}" class="btn btn-primary">Back</a>
