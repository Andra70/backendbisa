@extends('dashboard.layouts.main')

@section('content')

<h2>Job Category</h2>
    <table class="table">
        <h2>Job Categories</h2>
        <!-- Add New Job Category Button -->
        <a href="{{ route('job_categories.create') }}" class="btn btn-primary">Add New Job Category</a>
        <!-- Table Header -->
        <thead>
            <tr>
                <th>ID</th>
                <th>Job Category</th>
                <th>Action</th>
            </tr>
        </thead>
        <!-- Table Body -->
        <tbody>
            @foreach ($jobCategories as $jobCategory)
            <tr>
                <td>{{ $jobCategory->id }}</td>
                <td>{{ $jobCategory->name }}</td>
                <td>
                    <a href="{{ route('job_categories.show', ['id' => $jobCategory->id]) }}" class="btn btn-info">View Job Category</a>
                    <!-- Edit Job Category Link -->
                    <a href="{{ route('job_categories.update', ['job_category' => $jobCategory->id]) }}" class="btn btn-primary">Edit Job Category</a>
                    <!-- Delete Job Category Form -->
                    <form action="{{ route('job_categories.destroy', ['job_category' => $jobCategory->id]) }}" method="POST" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this job category?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>