<!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>copyright &copy; <script>
                                document.write(new Date().getFullYear());
                            </script> - developed by
                            <b><a href="https://indrijunanda.gitlab.io/" target="_blank">Me</a></b>
                        </span> , distributed by
                        <b><a href="https://themewagon.com/" target="_blank">Me</a></b>
                        </span>
                        </span> , modded and used by
                        <b><a href="" target="_blank">BISA App Team</a></b>
                        </span>
                    </div>
                </div>

            </footer>
            <!-- Footer -->