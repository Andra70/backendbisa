@extends('layouts.main')

@section('content')
    <div class="container">
        <h2>Tests</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Rating</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tests as $test)
                <tr>
                    <td>{{ $test->name }}</td>
                    <td>{{ $test->status }}</td>
                    <td>{{ $test->rating }}</td>
                    <td>
                        <!-- Edit Link -->
                        <a href="{{ route('tests.update', ['test' => $test->id]) }}" class="btn btn-primary">Edit</a>
                        <!-- Delete Form -->
                        <form action="{{ route('tests.destroy', ['test' => $test->id]) }}" method="POST" class="d-inline">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this test?')">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
