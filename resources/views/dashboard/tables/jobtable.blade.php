@extends('layouts.main')

@section('content')
    <div class="container">
        <h2>Job Categories</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Job Category</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobCategories as $jobCategory)
                <tr>
                    <td>{{ $jobCategory->name }}</td>
                    <td>
                        <!-- Edit Link -->
                        <a href="{{ route('job_categories.update', ['job_category' => $jobCategory->id]) }}" class="btn btn-primary">Edit</a>
                        <!-- Delete Form -->
                        <form action="{{ route('job_categories.destroy', ['job_category' => $jobCategory->id]) }}" method="POST" class="d-inline">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this job category?')">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
