<?php

namespace Database\Seeders;

use App\Models\Quiz;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class QuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $quizzes = [
            [
                'name' => 'Test Minat',
                'status' => 'active',
                'rating' => 4,
            ],
            [
                'name' => 'Test Bakat',
                'status' => 'active',
                'rating' => 3,
            ],
            [
                'name' => 'Test Minat',
                'status' => 'active',
                'rating' => 4,
            ],
            [
                'name' => 'Test Bakat',
                'status' => 'active',
                'rating' => 3,
            ],
            // Add more quizzes as needed
        ];

        foreach ($quizzes as $quiz) {
            Quiz::create($quiz);
        }
    }
}
