<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $questions = [
            [
                'quiz_id' => 1, // from table 'quizzes'
                'question' => '1. At a party do you',    
            ],
            [
                'quiz_id' => 1, // from table 'quizzes'
                'question' => '2. Are you more',    
            ],
            [
                'quiz_id' => 1, // from table 'quizzes'
                'question' => '3. Is it worse to',    
            ],
            [
                'quiz_id' => 2, // from table 'quizzes'
                'question' => '1. Are you more impressed by',    
            ],
            [
                'quiz_id' => 2, // from table 'quizzes'
                'question' => '2. Are you more drawn toward the',    
            ],
            [
                'quiz_id' => 2, // from table 'quizzes'
                'question' => '3. Do you prefer to work',    
            ],
            [
                'quiz_id' => 3, // from table 'quizzes'
                'question' => '1. Do you tend to choose',    
            ],
            [
                'quiz_id' => 3, // from table 'quizzes'
                'question' => '2. At parties do you',  
            ],
            [
                'quiz_id' => 3, // from table 'quizzes'
                'question' => '3. Are you more attracted to',
            ],
        ]; 
        
        foreach ($questions as $question) {
            Question::create($question);
        }
    }
}
