<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        Subscription::create([
            'name' => 'silver',
            'description' => 'Paket Silver',
            'price' => '10000'
        ]);
        Subscription::create([
            'name' => 'gold',
            'description' => 'Paket Gold',
            'price' => '20000'
        ]);
        Subscription::create([
            'name' => 'platinum',
            'description' => 'Paket Platinum',
            'price' => '30000'
        ]);
    }
}
