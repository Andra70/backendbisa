<?php

namespace Database\Seeders;

use App\Models\Answer;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class AnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $answers = [
            [
                'question_id' => 1,
                'answer' => 'a',
                'mbti_type' => 'E',
                'answers' => 'b',
                'mbti_types' => 'I',
            ],
            [
                'question_id' => 2,
                'answer' => 'a',
                'mbti_type' => 'E',
                'answers' => 'b',
                'mbti_types' => 'I',
            ],
            [
                'question_id' => 3,
                'answer' => 'a',
                'mbti_type' => 'S',
                'answers' => 'b',
                'mbti_types' => 'N',
            ],
            [
                'question_id' => 4,
                'answer' => 'a',
                'mbti_type' => 'T',
                'answers' => 'b',
                'mbti_types' => 'F',
            ],
            [
                'question_id' => 5,
                'answer' => 'a',
                'mbti_type' => 'T',
                'answers' => 'b',
                'mbti_types' => 'F',
            ],
            [
                'question_id' => 6,
                'answer' => 'a',
                'mbti_type' => 'T',
                'answers' => 'b',
                'mbti_types' => 'F',
            ],
        ];
        foreach ($answers as $answer) {
            Answer::create($answer);
        }
    }
}