<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MbtiTestSeeder extends Seeder
{
    public function run()
    {
        $questions = [
            [
                'soal' => '1. At a party do you',
                'answers' => 'a',
                'mbti_types' => 'E',
                'answer' => 'b',
                'mbti_type' => 'I',
                'score' => 2,
            ],
            
            [
                'soal' => '2. Are you more',
                'answers' => 'a',
                'mbti_types' => 'E',
                'answer' => 'b',
                'mbti_type' => 'I',
                'score' => 2,
            ],
            
            [
                'soal' => '3. Is it worse to',
                'answers' => 'a',
                'mbti_types' => 'S',
                'answer' => 'b',
                'mbti_type' => 'N',
                'score' => 2,
            ],
            [
                'soal' => '4. Are you more impressed by',
                'answers' => 'a',
                'mbti_types' => 'S',
                'answer' => 'b',
                'mbti_type' => 'N',
                'score' => 2,
            ],
            
            [
                'soal' => '5. Are you more drawn toward the',
                'answers' => 'a',
                'mbti_types' => 'T',
                'answer' => 'b',
                'mbti_type' => 'F',
                'score' => 2,
            ],
            
            [
                'soal' => '6. Do you prefer to work',
                'answers' => 'a',
                'mbti_types' => 'T',
                'answer' => 'b',
                'mbti_type' => 'F',
                'score' => 2,
            ],
            [
                'soal' => '7. Do you tend to choose',
                'answers' => 'a',
                'mbti_types' => 'J',
                'answer' => 'b',
                'mbti_type' => 'P',
                'score' => 2,
            ],
            
            [
                'soal' => '8. At parties do you',
                'answers' => 'a',
                'mbti_types' => 'E',
                'answer' => 'b',
                'mbti_type' => 'I',
                'score' => 2,
            ],
            
            [
                'soal' => '9. Are you more attracted to',
                'answers' => 'a',
                'mbti_types' => 'E',
                'answer' => 'b',
                'mbti_type' => 'I',
                'score' => 2,
            ],

            [
                'soal' => '10. Are you more interested in',
                'answers' => 'a',
                'mbti_types' => 'S',
                'answer' => 'b',
                'mbti_type' => 'N',
                'score' => 2,
            ]

            
            
            ];

        foreach ($questions as $question) {
            DB::table('mbti_tests')->insert($question);
        }
    }
}
